vim.opt.spelllang = "en_gb"
vim.opt.guicursor = ""
vim.opt.mouse = ""

-- Line numbers
vim.opt.nu = true
vim.opt.relativenumber = true

-- Indentation and tabbing
vim.opt.tabstop = 2
vim.opt.softtabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.smartindent = true
vim.opt.autoindent = true

vim.opt.wrap = false

vim.opt.swapfile = false
vim.opt.backup = false
vim.opt.undodir = os.getenv("HOME") .. "/.local/share/nvim/undodir"
vim.opt.undofile = true

-- Search
vim.opt.hlsearch = false
vim.opt.incsearch = true
vim.opt.smartcase = true


vim.opt.termguicolors = true

-- Scroll
vim.opt.scrolloff = 8
vim.opt.signcolumn = "yes"
vim.opt.isfname:append("@-@")

vim.opt.updatetime = 50

-- Column indicator
vim.opt.colorcolumn = "+1"
vim.opt.textwidth = 80

-- Window splitting
vim.opt.splitbelow = true
vim.opt.splitright = true

vim.opt.lazyredraw = true

-- Wild menu
vim.opt.wildmode = "longest,list,full"

vim.opt.foldmethod = "manual"
