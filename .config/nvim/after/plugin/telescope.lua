local builtin = require('telescope.builtin')
-- Files search
vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
-- Git files search
vim.keymap.set('n', '<C-p>', builtin.git_files, {})
-- Project search
vim.keymap.set('n', '<leader>ps', function()
	builtin.grep_string({ search = vim.fn.input("Grep > ") });
end)
