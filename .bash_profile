# Set environment variables
[ -f $HOME/.config/env ] && source $HOME/.config/env

[ -d $HOME/.local/bin ] && PATH="$HOME/.local/bin:$PATH"
[ -d $HOME/.local/share/mypro/nodepkgs/node_modules/.bin ] && PATH="$HOME/.local/share/mypro/nodepkgs/node_modules/.bin:$PATH"
[ -d $HOME/.local/golib ] && PATH="$HOME/.local/golib/bin:$PATH"
[ -f "/home/rava/.ghcup/env" ] && source "/home/rava/.ghcup/env" # ghcup-env
[ -f "/usr/share/fzf/key-bindings.bash" ] && source "/usr/share/fzf/key-bindings.bash"
[ -f "/usr/share/fzf/completion.bash" ] && source "/usr/share/fzf/completion.bash"

[ -f $HOME/.bashrc ] && . $HOME/.bashrc

[ "$(tty)" = "/dev/tty1" ] && exec startx
