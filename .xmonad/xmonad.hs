--IMPORTS
import XMonad
import Data.Monoid
import System.Exit
import Graphics.X11.Xlib.Cursor
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageHelpers
import XMonad.Layout.Spacing
import XMonad.Layout.NoBorders

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal :: String
myTerminal      = "$TERMINAL"
--myFm :: String
--myFm = "vifm"


-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 2

myModMask       = mod4Mask

-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
myWorkspaces    = ["\61461", "\61728", "\61501", "\61729", "\62610", "\62161", "\61502", "\63159", "\61612"]
-- ++ map show [1..9]

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#1d2021"
myFocusedBorderColor = "#a89984"
------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $

    -- launch a terminal
    [ ((modm,               xK_Return), spawn $ XMonad.terminal conf)

    -- launch dmenu
    --, ((modm,               xK_p     ), spawn "dmenu_run")

    -- launch passmenu
    --, ((modm .|. shiftMask, xK_i     ), spawn "passmenu --type")

    -- close focused window
    , ((modm, xK_c     ), kill)

     -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    --  Reset the layouts on the current workspace to default
    , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)

    -- Resize viewed windows to the correct size
    , ((modm,               xK_n     ), refresh)

    -- Move focus to the next window
    --, ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the next window
    , ((modm,               xK_j     ), windows W.focusDown)

    -- Move focus to the previous window
    , ((modm,               xK_k     ), windows W.focusUp  )

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm,               xK_y), windows W.swapMaster)

    -- Swap the focused window with the next window
    , ((modm .|. shiftMask, xK_j     ), windows W.swapDown  )

    -- Swap the focused window with the previous window
    , ((modm .|. shiftMask, xK_k     ), windows W.swapUp    )

    -- Shrink the master area
    , ((modm,               xK_h     ), sendMessage Shrink)

    -- Expand the master area
    , ((modm,               xK_l     ), sendMessage Expand)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_i ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_u), sendMessage (IncMasterN (-1)))

    -- Toggle the status bar gap
    -- Use this binding with avoidStruts from Hooks.ManageDocks.
    -- See also the statusBar function from Hooks.DynamicLog.
    --
    -- , ((modm              , xK_b     ), sendMessage ToggleStruts)

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm              , xK_q     ), spawn "xmonad --recompile; xmonad --restart")

    -- Volume controls
    --, ((modm, xK_equal), spawn "pamixer -i 5")
    --, ((modm, xK_minus), spawn "pamixer -d 5")
    --, ((modm .|. shiftMask, xK_equal), spawn "pamixer -i 2")
    --, ((modm .|. shiftMask, xK_minus), spawn "pamixer -d 2")
    --, ((modm .|. shiftMask, xK_m), spawn "$HOME/.local/bin/scripts/mute")
    --, ((modm, xK_Print), spawn "$HOME/.local/bin/dmenuscripts/recorder")
    --, ((modm, xK_F11), spawn "$HOME/.local/bin/dmenuscripts/sys_reboot")
    --, ((modm, xK_F12), spawn "$HOME/.local/bin/dmenuscripts/sys_poweroff")
-- , ((modm, xK_e), (spawn (myTerminal ++ (" $HOME/.local/bin/dmenuscripts/vifmshort"))))
    --, ((modm, xK_e), spawn " $HOME/.local/bin/dmenuscripts/vifmshort")

    , ((modm, xK_f),    sendMessage ToggleStruts)
    ]
    ++

    --
    -- mod-[1..9], Switch to workspace N
    -- mod-shift-[1..9], Move client to workspace N
    --
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
--    ++
--
--
--     mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
--     mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
--
--    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
--        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
--        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = spacingRaw True (Border 0 5 5 5) True (Border 5 5 5 5) True $ avoidStruts $ tiled ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   =  Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = composeAll
    [ className =? "mpv"                   --> doFloat,
      className =? "Sxiv"                  --> doFloat,
      className =? "Zathura"               --> doShift (myWorkspaces !! 7),
      className =? "Gimp"                  --> doShift ( myWorkspaces !! 6 ),
      className =? "Gimp"                  --> doFullFloat,
      className =? "gimp*"                 --> doFloat,
      (className =? "Code" <&&> title =? "Open Folder") --> doFloat,
      (className =? "Code" <&&> resource =? "Open Folder") --> doFloat,
      (className =? "Code" <&&> resource =? "Visual Studio Code") --> doFloat,
      className =? "Code"                  --> doShift ( myWorkspaces !! 3 ),
      className =? "Code"                  --> doFullFloat,
      --title =? "Open File"                 --> doFullFloat,
      className =? "Brave"                 --> doFloat,
      className =? "Brave-browser"         --> doShift  ( myWorkspaces !! 8 ),
      className =? "Brave-browser"         --> doFullFloat,
      (className =? "firefox" <&&> resource =? "Dialog") --> doFloat,
      className =? "firefox"               --> doShift  ( myWorkspaces !! 6 ),
      className =? "firefox"               --> doFullFloat,
--      className =? "VirtualBox Manager"    --> doShift  ( myWorkspaces !! 5 ),
--      className =? "VirtualBox Manager"    --> doFloat,
--      className =? "VirtualBox Machine"    --> doShift  ( myWorkspaces !! 4 ),
--      className =? "VirtualBox Machine"    --> doFloat,
--      className =? "Audacity"              --> doFloat,
      className =? "dialog"                --> doFloat,
      className =? "splash"          --> doFloat,
      --className =? "firefoxdeveloperedition"    --> doShift  ( myWorkspaces !! 0 ),
      (className =? "firefoxdeveloperedition" <&&> resource =? "Dialog") --> doFloat,
      resource  =? "desktop_window"        --> doIgnore,
      resource  =? "kdesktop"              --> doIgnore ]

------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook = mempty

------------------------------------------------------------------------
-- Status bars and logging

-- Perform an arbitrary action on each internal state change or X event.
-- See the 'XMonad.Hooks.DynamicLog' extension for examples.
--
myLogHook = return ()

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
setDefaultCursor :: Glyph -> X ()
setDefaultCursor glyph = do
    dpy <- asks display
    rootw <- asks theRoot
    liftIO $ do
        curs <- createFontCursor dpy glyph
        defineCursor dpy rootw curs
        flush dpy
        freeCursor dpy curs

myStartupHook = setDefaultCursor xC_left_ptr


------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
_windowTitle = xmobarColor "#ebdbb2" "" . shorten 100
_currentWindow = xmobarColor "#ebdbb2" "" . wrap "<fn=0>[</fn><fn=2>" "</fn><fn=0>]</fn>"
_activeWindow = xmobarColor "#ebdbb2" "" . wrap "*<fn=2>" "</fn> "
_inActiveWindow = xmobarColor "#ebdbb2" "" . wrap " <fn=2>" "</fn> "
_sep = "<fc=#ebdbb2> | </fc>"
_urgentWindow = xmobarColor "#ebdbb2" "" . wrap "!<fn=2>" "</fn>"
_xmobar h = xmobarPP {
  ppOutput = hPutStrLn h,
  ppTitle = _windowTitle,
  ppHidden = _activeWindow,
  ppHiddenNoWindows = _inActiveWindow,
  ppSep = _sep,
  ppUrgent = _urgentWindow,
  ppVisible = xmobarColor "#ffffff" "",
  ppCurrent = _currentWindow,
  ppLayout  = (\layout -> case layout of
    "Spacing Tall" -> "[|]"
    "Mirror Tall" -> "[-]"
    "ThreeCol"    -> "[||]"
    "Tabbed"      -> "[_]"
    "Gimp"        -> "[&]"
    )

  }
main = do
  h <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc"
  xmonad $ docks $ defaults {
    logHook = dynamicLogWithPP $ _xmobar h,
    manageHook         = manageDocks <+> manageHook defaults,
    layoutHook         = avoidStruts  $ layoutHook defaults
    -- handleEventHook    = handleEventHook defaults <+> docksEventHook
  }
-- =<< xmobar (docks defaults)
    --xmproc <- spawnPipe "xmobar -x 0 $HOME/.config/xmobar/xmobarrc"
    --xmonad $ docks defaults
-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
defaults = def {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- key bindings
        keys               = myKeys,
        mouseBindings      = myMouseBindings,

      -- hooks, layouts
        layoutHook         = smartBorders myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        logHook            = myLogHook,
        startupHook        = myStartupHook
    }

-- | Finally, a copy of the default bindings in simple textual tabular format.
help :: String
help = unlines ["The default modifier key is 'alt'. Default keybindings:",
    "",
    "-- launching and killing programs",
    "mod-Shift-Enter  Launch xterminal",
    "mod-p            Launch dmenu",
    "mod-Shift-p      Launch gmrun",
    "mod-Shift-c      Close/kill the focused window",
    "mod-Space        Rotate through the available layout algorithms",
    "mod-Shift-Space  Reset the layouts on the current workSpace to default",
    "mod-n            Resize/refresh viewed windows to the correct size",
    "",
    "-- move focus up or down the window stack",
    "mod-Tab        Move focus to the next window",
    "mod-Shift-Tab  Move focus to the previous window",
    "mod-j          Move focus to the next window",
    "mod-k          Move focus to the previous window",
    "mod-m          Move focus to the master window",
    "",
    "-- modifying the window order",
    "mod-Return   Swap the focused window and the master window",
    "mod-Shift-j  Swap the focused window with the next window",
    "mod-Shift-k  Swap the focused window with the previous window",
    "",
    "-- resizing the master/slave ratio",
    "mod-h  Shrink the master area",
    "mod-l  Expand the master area",
    "",
    "-- floating layer support",
    "mod-t  Push window back into tiling; unfloat and re-tile it",
    "",
    "-- increase or decrease number of windows in the master area",
    "mod-comma  (mod-,)   Increment the number of windows in the master area",
    "mod-period (mod-.)   Deincrement the number of windows in the master area",
    "",
    "-- quit, or restart",
    "mod-Shift-q  Quit xmonad",
    "mod-q        Restart xmonad",
    "mod-[1..9]   Switch to workSpace N",
    "",
    "-- Workspaces & screens",
    "mod-Shift-[1..9]   Move client to workspace N",
    "mod-{w,e,r}        Switch to physical/Xinerama screens 1, 2, or 3",
    "mod-Shift-{w,e,r}  Move client to screen 1, 2, or 3",
    "",
    "-- Mouse bindings: default actions bound to mouse events",
    "mod-button1  Set the window to floating mode and move by dragging",
    "mod-button2  Raise the window to the top of the stack",
    "mod-button3  Set the window to floating mode and resize by dragging"]
